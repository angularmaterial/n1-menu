import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule, MatMenuModule } from '@angular/material';

@NgModule({
  imports: [
    NoopAnimationsModule,
    MatIconModule,
    MatMenuModule
  ],
  exports: [
    NoopAnimationsModule,
    MatIconModule,
    MatMenuModule
  ],
})
export class MaterialComponentsModule { }
