import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { MatTab, MatTabGroup, MatTabNav, MatTabLink } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Material Component Demo - Menu';

  navLinks = [{
    path: '/a',
    label: 'a'
  },{
    path: '/b',
    label: 'b'
  }];

}
